package myFirstSpringKotlinApp1.controllers

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping( "/calculator")
class CalculatorController {
    @GetMapping("/mult")
    fun mult(n1 : Int , n2 : Int) = n1*n2
}